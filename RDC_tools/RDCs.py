from RDC_tools import file_io

import numpy as np

from .gamma import gammas

import numpy as np

# RDCs: Hz
# [distance] = angstrom
# [gamma] = 10^6 * rad /(s * T)

# 3 / (8*pi^2) * mu_0 * hbar * 1e6^2 * 1e10^3
b_prefactor = 5.035209531454766

def calc_directions(coupling_spec, positions):
    directions = []
    for average in coupling_spec:
        dirs = []
        for pair in average['orientations']:
            pos1 = positions[pair[0]]
            pos2 = positions[pair[1]]
            r = pos2-pos1
            r = r / np.linalg.norm(r)
            dirs.append(r)
        directions.append(np.array(dirs))

    return directions

def calc_RDCs(prefactors, distances, angle_parameters):
    if not len(prefactors) == len(distances) == len(angle_parameters):
        raise ValueError('prefactors, directions and angle_parameters need to have the same length')
    RDCs = np.empty(len(prefactors), dtype='float64')
    for i, (p, d, a) in enumerate(zip(prefactors, distances, angle_parameters)):
        RDCs[i] = np.mean(np.array(p) / np.array(d)**3 * np.array(a))
    return RDCs

def calc_prefactors(coupling_spec):
    prefactors = []
    for average in coupling_spec:
        w = []
        for pair, isotopes, scaling in zip(average['pairs'], average['pair_isotopes'], average['scalings']):
            i1 = isotopes[0]
            i2 = isotopes[1]
            w.append(-b_prefactor * scaling * gammas[i1] * gammas[i2])

        prefactors.append(np.array(w))

    return prefactors

def calc_distances(coupling_spec, positions):
    distances = []
    for average in coupling_spec:
        d = []
        for pair in average['pairs']:
            p1 = positions[pair[0]]
            p2 = positions[pair[1]]
            d.append(np.linalg.norm(p2-p1))

        distances.append(np.array(d))

    return distances

def calc_angle_parameters(coupling_spec, positions, ref=np.array([0.,0.,1.])):
    angle_parameters = []
    for average in coupling_spec:
        a = []
        for pair in average['orientations']:
            p1 = positions[pair[0]]
            p2 = positions[pair[1]]
            diff = p2 - p1
            cos = diff.dot(ref) / (np.linalg.norm(diff) * np.linalg.norm(ref))
            a.append(cos**2 - 1/3)
        angle_parameters.append(a)

    return angle_parameters

def angle_parameters_from_A(coupling_spec, A_matrix, pos_ref):
    angle_parameters = []
    for average in coupling_spec:
        a = []
        for pair in average['orientations']:
            p1 = pos_ref[pair[0]]
            p2 = pos_ref[pair[1]]
            r = (p2 - p1) / np.linalg.norm(p2-p1)
            a.append(r.dot(A_matrix.dot(r)))
        angle_parameters.append(a)

    return angle_parameters


def scaling_factor(values_to, values):
    A = np.array(values).reshape((len(values), 1))
    return np.linalg.lstsq(A, values_to, rcond=None)[0][0]

def similarity(values1, values2, w=None):
    """Cosine similarity

    Optionally weighted by w
    """
    if w is None:
        w = np.ones_like(values1)
    return np.sum(values1*values2*w)/np.sqrt(np.sum(w*values1**2)*np.sum(w*values2**2))
