from . import alignment_tensor, RDCs, file_io

import numpy as np

import argparse
import json
import sys

def fit_alignment_tensor(prog, argv):
    parser = argparse.ArgumentParser()
    parser.add_argument('couplings_file')
    parser.add_argument('xyz_file')
    parser.add_argument('RDC_file')
    args = parser.parse_args(argv)

    with open(args.couplings_file) as file:
        spec = json.load(file)

    couplings = file_io.parse_coupling_spec(spec)

    with open(args.xyz_file) as file:
        mols = file_io.read_xyz_file(file)

    with open(args.RDC_file) as file:
        mean, meanerr, _, _ = file_io.read_RDC_file(file)

    M = 0
    for atoms in mols:
        prefactors = RDCs.calc_prefactors(couplings)
        directions = RDCs.calc_directions(couplings, [a['pos'] for a in atoms])
        distances = RDCs.calc_distances(couplings, [a['pos'] for a in atoms])
        M += alignment_tensor.angle_parameter_matrix(prefactors, directions, distances)

    M /= len(mols)

    A, residual, _ = alignment_tensor.fit_alignment_tensor_averages(M, mean, meanerr)

    print('Weighted:')
    for i in range(3):
        print(f'    {A[i,0]:>14.5e} {A[i,1]:>14.5e} {A[i,2]:>14.5e}')
    print(f'    Sum of squared residuals: {residual}')

    A, residual = alignment_tensor.fit_alignment_tensor_averages(M, mean)

    print('Unweighted:')
    for i in range(3):
        print(f'    {A[i,0]:>14.5e} {A[i,1]:>14.5e} {A[i,2]:>14.5e}')
    print(f'    Sum of squared residuals: {residual}')

    Dnew = alignment_tensor.calc_RDCs(M, A)
    print('RDCs: [input]      [fitted]        [diff]         [err]    [diff/err]')
    for i in range(len(mean)):
        diff = np.abs(Dnew[i]-mean[i])
        print(f'{mean[i]:>13.3e} {Dnew[i]:>13.3e} {diff:>13.3e} {meanerr[i]:>13.3e} {diff/meanerr[i]:>13.3e}')

def similarity(prog, argv):
    parser = argparse.ArgumentParser()
    parser.add_argument('values1')
    parser.add_argument('values2')
    args = parser.parse_args(argv)

    v1 = np.loadtxt(args.values1)
    v2 = np.loadtxt(args.values2)

    def detect_shape(v):
        if v.shape == (3,3):
            return alignment_tensor.from_matrix(v)
        else:
            return v

    v1 = detect_shape(v1)
    v2 = detect_shape(v2)

    sim = RDCs.similarity(v1, v2)
    scale_factor = RDCs.scaling_factor(v2, v1)

    print(f'Similarity = {sim}')
    print(f'values2 = {scale_factor:.3f} * values1')

if __name__ == '__main__':
    choices = {
            'fit_alignment_tensor': fit_alignment_tensor,
            'similarity': similarity,
    }

    parser = argparse.ArgumentParser(prog='RDC_tools')
    parser.add_argument('command', choices=choices.keys())

    args = parser.parse_args(sys.argv[1:2])

    choices[args.command]('RDC_tools ' + args.command, sys.argv[2:])
