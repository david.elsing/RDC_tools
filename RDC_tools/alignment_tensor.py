from . import RDCs

import numpy as np

def to_matrix(values):
    return np.array([[-values[0]-values[1], values[2], values[3]],
                  [values[2], values[0], values[4]],
                  [values[3], values[4], values[1]]])

def from_matrix(A_matrix, rtol=1e-6, check=True):
    if check:
        max_val = np.max(np.abs(A_matrix))
        np.testing.assert_allclose(0, np.sum(np.diag(A_matrix)), rtol=0, atol=rtol * max_val)
        np.testing.assert_allclose(A_matrix, A_matrix.T, rtol=rtol)
    return np.array([A_matrix[1,1], A_matrix[2,2], A_matrix[0,1], A_matrix[0,2], A_matrix[1,2]])

def angle_parameter_matrix(prefactors, directions, distances):
    """Gives the matrix M such that
    (M * (A_22, A_33, A_12, A_13, A_23))_i = r_i^T A r_i
    for the bond directions r_i in the molecule
    """
    M = np.empty((len(directions), 5), dtype='float64')
    for i, (r, d, w) in enumerate(zip(directions, distances, prefactors)):
        r = np.atleast_2d(r)
        d = np.atleast_1d(d)
        w = np.atleast_1d(w)
        M[i,0] = np.mean(w / d**3 * (r[:,1]**2-r[:,0]**2))
        M[i,1] = np.mean(w / d**3 * (r[:,2]**2-r[:,0]**2))
        M[i,2] = np.mean(w / d**3 * (2*r[:,0]*r[:,1]))
        M[i,3] = np.mean(w / d**3 * (2*r[:,0]*r[:,2]))
        M[i,4] = np.mean(w / d**3 * (2*r[:,1]*r[:,2]))
    return M

def fit_alignment_tensor_averages(M, RDCs, errors=None):
    """Fit the alignment tensor components to RDCs

    Parameters
    ----------
    M: (N,5) ndarray
        The angle parameter matrix
    RDCs: (N,) array_like
        The RDCs
    errors: (N,) array_like, optional
        The measurement errors of the RDCs

    Returns
    -------
    (3,3) ndarray
        The alignment tensor
    """
    RDCs = np.array(RDCs)

    if errors is None:
        v, residual, *_ = np.linalg.lstsq(M, RDCs, rcond=None)
    else:
        w = 1 / errors
        X = w[:,None] * M
        b = w * RDCs
        v, residual, *_ = np.linalg.lstsq(X, b, rcond=None)
        Cov = np.linalg.inv(X.T.dot(X))

    A = to_matrix(v)

    if errors is None:
        return A, np.sum(residual)
    else:
        return A, np.sum(residual), Cov

def natural_abundance_mass(name):
    """Atom masses at natural abundance

    For this case, the names are sufficient
    """
    return {
        'C': 12.011,
        'H': 1.008,
        'N': 14.007
    }[name[0]]

def from_inertia_tensor(atoms):
    """Calculate the alignment tensor as proportional to the inertia tensor
    """
    r = np.array([a['pos'] for a in atoms])
    m = np.array([natural_abundance_mass(a['name']) for a in atoms])
    M = np.sum(m)
    r_com = np.sum(r*m[:,None], axis=0) / M
    r = r - r_com[None,:]
    rsq = np.sum(r**2, axis=1)
    xixj = np.einsum('...i,...j->...ij', r, r)
    I = np.sum(m[:,None,None] * (rsq[:,None,None] * np.eye(3)[None,:,:] - xixj), axis=0) / M
    A = np.eye(3)*np.trace(I) - 3*I
    return A

def from_gyration_tensor(atoms):
    """Calculate the alignment tensor from the "gyration" tensor

    The positions are weighted by mass as for the inertia tensor
    """
    r = np.array([a['pos'] for a in atoms])
    m = np.array([natural_abundance_mass(a['name']) for a in atoms])
    M = np.sum(m)
    r_com = np.sum(r*m[:,None], axis=0) / M
    r = r - r_com[None,:]
    Rsq = np.einsum('ki,kj,k->ij', r, r, m) / M
    lam, Q = np.linalg.eigh(Rsq)
    rho = np.sqrt(lam[::-1])
    if rho[0] == rho[2]:
        return np.zeros_like(Rsq)
    delta = (rho[1] - rho[2]) / (rho[0] - rho[2])
    diag = [1-0.5*delta, delta-0.5, -0.5-0.5*delta][::-1]
    A = Q.dot(np.diag(diag).dot(Q.T))
    return A

def com(atoms):
    """Center of mass of atoms at natural abundance
    """
    res = 0
    M = 0
    for a in atoms:
        m = natural_abundance_mass(a['name'])
        res += np.array(a['pos'])*m
        M += m
    return res / M


def similarity(A1, A2):
    """Cosine similarity between alignment tensors
    """
    v1 = A1.flatten()
    v2 = A2.flatten()
    return np.sum(v1*v2)/np.sqrt(np.sum(v1**2)*np.sum(v2**2))

def calc_RDCs(M, A_matrix, ACov = None):
    """Calculate RDCs from the alignment_tensor for a rigid molecule

    If the covariance matrix is given, errors are propagated
    Parameters
    ----------
    M: (N,5) ndarray
        The angle parameter matrix
    A_matrix: (3,3) ndarray
        Alignment tensor
    ACov: (5,5) ndarray, optional
        Covariance matrix of the components (A_22,A_33,A_12,A_13,A_23)
    """
    D = M.dot(from_matrix(A_matrix))
    if ACov is None:
        return D
    else:
        Cov = M.dot(ACov.dot(M.T))
        return D, Cov
