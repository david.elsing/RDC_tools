import numpy as np

import json

def read_xyz_file(file):
    mols = []
    mode = 'start'
    for line in file:
        print(mode)
        print(line,end='')
        if mode == 'start':
            N = int(line.strip())
            mode = 'comment'
            continue
        elif mode == 'comment':
            mode = 'coord'
            atoms = []
            i = 0
            continue

        parts = line.split()
        atoms.append({
            'name': parts[0],
            'pos': np.array([float(v) for v in parts[1:4]])
            })

        i += 1
        if i == N:
            mols.append(atoms)
            mode = 'start'

    return mols

def _parse_normal(coupling, coupling_average):
    pair = [a['index']-1 for a in coupling['pair']]
    isotopes = [a['isotope'] for a in coupling['pair']]
    coupling_average['orientations'].append(pair)
    coupling_average['scalings'].append(1.0)
    coupling_average['pairs'].append(pair)
    coupling_average['pair_isotopes'].append(isotopes)

def _parse_projected(coupling, coupling_average):
    orientation = [i-1 for i in coupling['orientation']]
    scaling = coupling['scaling']
    for cp in coupling['pairs']:
        pair = [a['index']-1 for a in cp]
        isotopes = [a['isotope'] for a in cp]
        coupling_average['orientations'].append(orientation)
        coupling_average['scalings'].append(scaling)
        coupling_average['pairs'].append(pair)
        coupling_average['pair_isotopes'].append(isotopes)

def parse_coupling_spec(spec):
    averages = []
    for coupling in spec['data']:
        ctype = coupling['type']
        coupling_average = {
            'orientations': [],
            'scalings': [],
            'pairs': [],
            'pair_isotopes': []
        }

        if ctype == 'normal':
            _parse_normal(coupling, coupling_average)
        elif ctype == 'average':
            for coupling2 in coupling['couplings']:
                ctype2 = coupling2['type']
                if ctype2 == 'normal':
                    _parse_normal(coupling2, coupling_average)
                elif ctype2 == 'projected':
                    _parse_projected(coupling2, coupling_average)
                else:
                    raise RuntimeError(f'Invalid coupling type "{ctype2}"')
        elif ctype == 'projected':
            _parse_projected(coupling, coupling_average)
        else:
            raise RuntimeError(f'Invalid coupling type "{ctype}"')

        averages.append(coupling_average)

    return averages

def read_RDC_file(file):
    D = []
    Derr = []
    mean = []
    meanerr = []
    for line in file:
        parts = line.split()
        if parts[0] == 'a':
            values = []
            errvalues = []
            n = int(parts[1])
            for j in range(n):
                values.append(float(parts[2+2*j]))
                errvalues.append(float(parts[2+2*j+1]))
            D.append(np.array(values))
            Derr.append(np.array(errvalues))
            mean.append(np.mean(D[-1]))
            meanerr.append(np.mean(Derr[-1]))
        else:
            val = float(parts[0])
            valerr = float(parts[1])
            D.append([val])
            Derr.append(np.array([valerr]))
            mean.append(val)
            meanerr.append(valerr)

    return np.array(mean), np.array(meanerr), D, Derr


class Mol2File:
    class Atom:
        def __init__(self, name, pos, atom_type, res_index=None, res_name=None, charge=None):
            self.name = name
            self.pos = pos
            self.atom_type = atom_type
            self.res_index = res_index
            self.res_name = res_name
            self.charge = charge

        def __repr__(self):
            return f'Atom(name = {self.name}, pos = {self.pos}, atom_type = {self.atom_type}, res_index = {self.res_index}, res_name = {self.res_name}, charge = {self.charge})'

    class Bond:
        def __init__(self, index1, index2, bond_type):
            self.index1 = index1
            self.index2 = index2
            self.bond_type = bond_type

    class Header:
        def __init__(self, name, mol_type, charge_type):
            self.name = name
            self.mol_type = mol_type
            self.charge_type = charge_type

    def __init__(self, header, atoms, bonds):
        self.header = header
        self.atoms = atoms
        self.bonds = bonds

    @classmethod
    def parse(cls, lines):
        self = cls.__new__(cls)
        mode = None
        self.atoms = []
        self.atom_indices = {}
        tmp_bonds = []
        header_count = 0
        i = 0
        for line in lines:
            if line.startswith('@<TRIPOS>MOLECULE'):
                mode = 'header'
            elif line.startswith('@<TRIPOS>ATOM'):
                mode = 'atom'
            elif line.startswith('@<TRIPOS>BOND'):
                mode = 'bond'
            else:
                if mode == 'atom':
                    parts = line.split()
                    self.atoms.append(Mol2File.Atom(
                        name = parts[1],
                        pos = np.array([float(v) for v in parts[2:5]]),
                        atom_type = parts[5],
                        res_index = None if len(parts) < 7 or parts[6] == '****' else int(parts[6]),
                        res_name = None if len(parts) < 8 or parts[7] == '****' else parts[7],
                        charge = None if len(parts) < 9 or parts[8] == '****' else float(parts[8]),
                    ))
                    self.atom_indices[int(parts[0])] = i
                    i += 1
                elif mode == 'bond':
                    parts = line.split()
                    tmp_bonds.append([
                        int(parts[1]), int(parts[2]), parts[3]
                    ])
                elif mode == 'header':
                    if header_count == 0:
                        name = line.strip('\n')
                    elif header_count == 2:
                        mol_type = line.strip('\n')
                    elif header_count == 3:
                        charge_type = line.strip('\n')

                    header_count += 1

        self.header = Mol2File.Header(name = name, mol_type = mol_type, charge_type = charge_type)

        self.bonds = []
        for b in tmp_bonds:
            self.bonds.append(Mol2File.Bond(
                index1 = self.atom_indices[b[0]],
                index2 = self.atom_indices[b[1]],
                bond_type = b[2]
            ))

        return self

    def save(self, file):
        file.write('@<TRIPOS>MOLECULE\n')
        h = self.header
        file.write(f'{h.name}\n{len(self.atoms)} {len(self.bonds)}\n{h.mol_type}\n{h.charge_type}\n')
        file.write('@<TRIPOS>ATOM\n')
        def f(value):
            if isinstance(value, float):
                return f'{value: >10.4f}'
            elif isinstance(value, str):
                return f'{value: >7}'
            elif isinstance(value, int):
                return f'{value: >7}'
            else:
                return str(value)
        for i, a in enumerate(self.atoms, start=1):
            s = f'{f(i)}{f(a.name)}{f(a.pos[0])}{f(a.pos[1])}{f(a.pos[2])}{f(a.atom_type)}'
            k = -1
            for j, v in enumerate([a.res_index, a.res_name, a.charge]):
                if v is not None:
                    k = j

            for j, v in enumerate([a.res_index, a.res_name, a.charge]):
                if v is None:
                    if j <= k:
                        s += ' ****'
                else:
                    s += f(v)

            file.write(s + '\n')

        file.write('@<TRIPOS>BOND\n')
        def f(value):
            return f'{value: >5}'
        for i, b in enumerate(self.bonds, start=1):
            file.write(f'{f(i)}{f(b.index1+1)}{f(b.index2+1)}{f(b.bond_type)}\n')
