from setuptools import setup


setup(
    name='RDC_tools',
    version='0.2',
    python_requires='>=3.6',
    author='David Elsing',
    author_email='david.elsing@kit.edu',
    description='Tools for analyzing RDCs',
    long_description='',
    install_requires=[
        'numpy',
        'scipy'
    ],
    packages=[
        'RDC_tools'
    ],
)
