from RDC_tools import RDCs

import pytest
import numpy as np

_scaling = 1.452
_scaling = 1

_coupling_spec = [
    {
        'orientations': [[0, 1]],
        'scalings': [_scaling],
        'pairs': [[0, 1]],
        'pair_isotopes': [['C13', 'H1']]
    }
]

class TestRDCs:
    def test_prefactors(self):
        prefactors = RDCs.calc_prefactors(_coupling_spec)

        assert np.allclose(prefactors[0], _scaling * (-90.6e3), rtol=1e-3)
