from RDC_tools import alignment_tensor

import pytest
import numpy as np

class TestInertiaTensor:
    def test_disk(self):
        positions = [[np.cos(alpha), np.sin(alpha), 0] for alpha in np.linspace(0, 2*np.pi, 21)[:-1]]
        atoms = [{'pos': p, 'name': 'C'} for p in positions]
        A = alignment_tensor.from_inertia_tensor(atoms)
        assert A[2,2] < 0
        np.testing.assert_allclose(-2*A[0,0], A[2,2], 1e-3)
        np.testing.assert_allclose(-2*A[1,1], A[2,2], 1e-3)

    def test_ellipsoid(self):
        theta = np.linspace(0, np.pi, 7)[:,None]
        phi = np.linspace(0, 2*np.pi, 7+1)[:-1][None,:]
        positions = np.array([
            np.sin(theta)*np.cos(phi), np.sin(theta)*np.sin(phi), 3*np.cos(theta)*np.ones_like(phi)
        ]).reshape((3,-1)).T
        atoms = [{'pos': p, 'name': 'C'} for p in positions]
        A = alignment_tensor.from_inertia_tensor(atoms)
        assert A[2,2] > 0
        rel = 1e-3
        np.testing.assert_allclose(-2*A[0,0], A[2,2], rel)
        np.testing.assert_allclose(-2*A[1,1], A[2,2], rel)
        assert np.abs(A[0,1]) < rel*np.abs(A[2,2])
        assert np.abs(A[0,2]) < rel*np.abs(A[2,2])
        assert np.abs(A[1,2]) < rel*np.abs(A[2,2])

class TestGyrationTensor:
    def test_disk(self):
        positions = [[np.cos(alpha), np.sin(alpha), 0] for alpha in np.linspace(0, 2*np.pi, 21)[:-1]]
        atoms = [{'pos': p, 'name': 'C'} for p in positions]
        A = alignment_tensor.from_gyration_tensor(atoms)
        print('A:',A)
        assert A[2,2] < 0
        np.testing.assert_allclose(-2*A[0,0], A[2,2], 1e-3)
        np.testing.assert_allclose(-2*A[1,1], A[2,2], 1e-3)

    def test_ellipsoid(self):
        theta = np.linspace(0, np.pi, 7)[:,None]
        phi = np.linspace(0, 2*np.pi, 7+1)[:-1][None,:]
        positions = np.array([
            np.sin(theta)*np.cos(phi), np.sin(theta)*np.sin(phi), 3*np.cos(theta)*np.ones_like(phi)
        ]).reshape((3,-1)).T
        atoms = [{'pos': p, 'name': 'C'} for p in positions]
        A = alignment_tensor.from_gyration_tensor(atoms)
        assert A[2,2] > 0
        rel = 1e-3
        np.testing.assert_allclose(-2*A[0,0], A[2,2], rel)
        np.testing.assert_allclose(-2*A[1,1], A[2,2], rel)
        assert np.abs(A[0,1]) < rel*np.abs(A[2,2])
        assert np.abs(A[0,2]) < rel*np.abs(A[2,2])
        assert np.abs(A[1,2]) < rel*np.abs(A[2,2])
