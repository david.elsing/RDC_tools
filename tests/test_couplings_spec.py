from RDC_tools import file_io

import pytest
import numpy as np

_couplings_spec = {
    "data": [
        {
            "type": "normal",
            "pair": [
                {"index": 2, "isotope": "C13"},
                {"index": 4, "isotope": "H1"}
            ],
        },
        {
            "type": "average",
            "couplings": [
                {
                    "type": "normal",
                    "pair": [
                        {"index": 5, "isotope": "C13"},
                        {"index": 7, "isotope": "C13"}
                    ]
                },
                {
                    "type": "normal",
                    "pair": [
                        {"index": 1, "isotope": "N15"},
                        {"index": 3, "isotope": "H3"}
                    ]
                }
            ],
        },
        {
            "type": "projected",
            "orientation": [2, 9],
            "scaling": -0.3333333333333333,
            "pairs": [
                [
                    {"index": 9, "isotope": "C13"},
                    {"index": 4, "isotope": "H1"}
                ],
                [
                    {"index": 7, "isotope": "C13"},
                    {"index": 18, "isotope": "F19"}
                ],
                [
                    {"index": 7, "isotope": "N14"},
                    {"index": 19, "isotope": "H1"}
                ]
            ],
        },
        {
            "type": "average",
            "couplings": [
                {
                    "type": "projected",
                    "orientation": [3, 2],
                    "scaling": 4.3,
                    "pairs": [
                        [
                            {"index": 10, "isotope": "C13"},
                            {"index": 24, "isotope": "H1"}
                        ],
                        [
                            {"index": 10, "isotope": "C13"},
                            {"index": 26, "isotope": "H1"}
                        ]
                    ]
                },
                {
                    "type": "projected",
                    "orientation": [5, 11],
                    "scaling": 2.3,
                    "pairs": [
                        [
                            {"index": 11, "isotope": "C13"},
                            {"index": 29, "isotope": "H1"}
                        ]
                    ]
                }
            ],
        }
    ]
}

@pytest.fixture
def couplings():
    return file_io.parse_coupling_spec(_couplings_spec)

class TestcouplingsSpec:
    def test_couplings_spec(self, couplings):
        assert len(couplings) == 4

    def test_normal(self, couplings):
        coupling = couplings[0]
        assert len(coupling['orientations']) == len(coupling['scalings']) \
            == len(coupling['pairs']) == len(coupling['pair_isotopes']) == 1

        assert coupling['orientations'] == coupling['pairs'] == [[1,3]]
        assert np.allclose(coupling['scalings'], 1.0)

    def test_average(self, couplings):
        coupling = couplings[1]
        assert len(coupling['orientations']) == len(coupling['scalings']) \
            == len(coupling['pairs']) == len(coupling['pair_isotopes']) == 2

        assert coupling['orientations'] == coupling['pairs'] == [[4,6], [0,2]]
        assert np.allclose(coupling['scalings'], [1.0, 1.0])

    def test_projected(self, couplings):
        coupling = couplings[2]
        assert len(coupling['orientations']) == len(coupling['scalings']) \
            == len(coupling['pairs']) == len(coupling['pair_isotopes']) == 3

        assert coupling['orientations'] == [[1,8], [1,8], [1,8]]
        assert coupling['pairs'] == [[8,3], [6,17], [6,18]]
        assert np.allclose(coupling['scalings'], [-1/3, -1/3, -1/3])

    def test_average_projected(self, couplings):
        coupling = couplings[3]
        assert len(coupling['orientations']) == len(coupling['scalings']) \
            == len(coupling['pairs']) == len(coupling['pair_isotopes']) == 3

        assert coupling['orientations'] == [[2,1], [2,1], [4,10]]
        assert coupling['pairs'] == [[9,23], [9,25], [10,28]]
        assert np.allclose(coupling['scalings'], [4.3, 4.3, 2.3])
